var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.SamsungInfolink = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    return this.playhead / 1000
  },

  /** Override to return video duration */
  getDuration: function () {
    if (this.playerType == 'INFOLINK-SEF') {
      return Math.round(this.player.Execute('GetDuration') / 1000)
    } else if (this.playerType == 'INFOLINK-PLAYER') {
      return Math.round(this.player.GetDuration() / 1000)
    }
    return null
  },

  /** Override to return current bitrate */
  getBitrate: function () {
    if (this.playerType == 'INFOLINK-SEF') {
      return this.player.Execute('GetCurrentBitrates')
    } else if (this.playerType == 'INFOLINK-PLAYER') {
      if (this.player.GetCurrentBitrate && this.player.GetCurrentBitrate()) {
        return this.player.GetCurrentBitrate()
      }
      if (this.player.GetCurrentBitrates && this.player.GetCurrentBitrates()) {
        return this.player.GetCurrentBitrates()
      }
    }
    return null
  },

  getRendition: function () {
    return youbora.Util.buildRenditionString(this.getBitrate()) || null
  },

  /** Override to return resource URL. */
  getResource: function () {
    var r = this.resource
    if (typeof r !== 'undefined') {
      if (r.indexOf('|') === -1) {
        return this.resource
      } else if (typeof r !== 'undefined') {
        return r.slice(0, r.indexOf('|'))
      }
    }
    return null
  },

  /** Override to return player version */
  getPlayerVersion: function () {
    return this.playerType
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'SAMSUNG ' + this.playerType
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    // Enable playhead monitor (buffer = true, seek = false)
    this.monitorPlayhead(true, true)
    try {
      /* Determine if INFOLINK or SEF */
      switch (this.player.getAttribute('classid')) {
        case 'clsid:SAMSUNG-INFOLINK-PLAYER':
          this.playerType = 'INFOLINK-PLAYER'
          break
        case 'clsid:SAMSUNG-INFOLINK-SEF':
          this.playerType = 'INFOLINK-SEF'
          break
        default:
          this.playerType = 'UNKNOWN'
      }
      youbora.Log.notice('Player type detected: ' + this.playerType)
    } catch (err) {
      youbora.Log.notice(err)
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()
  },

  playHandler: function (url) {
    this.resource = url
    this.fireStart()
  },

  errorHandler: function (err) {
    this.fireError(err, err)
    this.fireStop()
  },

  playtimeHandler: function (ms) {
    if (ms > 0.1) {
      this.fireJoin()
    }
    this.playhead = ms
  }
})

module.exports = youbora.adapters.SamsungInfolink
