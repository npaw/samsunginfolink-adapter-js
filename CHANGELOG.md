## [6.8.8] - 2024-11-28
## Fixed
-  Use node 14 when running the pipeline.

## [6.8.7] - 2024-11-28
## Changed
-  Use node 14 when running the pipeline.

## [6.8.6] - 2024-10-07
## Fixed
-  ES5 issues with new lib `6.8.58`.

## [6.8.5] - 2024-09-18
## Fixed
-  Deployment issue.

## [6.8.4] - 2024-09-17
### Changed
- Downgrade devDependencies to their original versions to make sure it work in old Smart TVs.
- Update `CHANGELOG.md`
### Library
- Packaged with `lib 6.8.57`

## [6.8.3] - 2024-09-16
### Library
- Packaged with `lib 6.5.15`

## [6.8.2] - 2024-09-16
### Library
- Packaged with `lib 6.5.15`

## [6.8.1] - 2024-09-16
### Library
- Packaged with `lib 6.5.26`

## [6.8.0] - 2024-08-29
### Library
- Packaged with `lib 6.8.57`
- Update webpack and webpack-cli versions
- Fix npm scripts

## [6.5.0] - 2019-09-23
### Library
- Packaged with `lib 6.5.15`

## [6.4.2] - 2019-04-08
### Fixed
- Monitor stop error
### Library
- Packaged with `lib 6.4.23`

## [6.4.1] - 2019-03-26
### Added
- Get bitrate using GetCurrentBitrate method
### Library
- Packaged with `lib 6.4.20`

## [6.4.0] - 2018-08-17
### Library
- Packaged with `lib 6.4.1`

## [6.3.0] - 2018-06-27
### Library
- Packaged with `lib 6.3.0`
